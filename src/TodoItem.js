import React, { useEffect, useState } from 'react';
import './TodoItem.css';

export function TodoItem(props) {
    const [item, setItem] = useState(props.taskItem);
    const [isCompleted, setIsCompleted] = useState(props.taskItem.isComplete);
    const handleCheck = (e) => {

        item.isComplete = e.target.checked;
        setItem(item);
        setIsCompleted(e.target.checked);
    }

    useEffect(() => {
        props.check(item.isComplete);
    }, [isCompleted]);
    return (
        <div className="divList" style={isCompleted ? { textDecoration: 'line-through' } : {}}>
            <input type="checkbox" name="cbComplete" checked={isCompleted} onChange={handleCheck} />
            <div className="displayTitle">{props.taskItem.inpAdd}</div>
            <div className="lblDel" onClick={props.onRemove}>Delete</div>
        </div>
    );
}

