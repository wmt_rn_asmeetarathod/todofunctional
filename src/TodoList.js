import React, { useRef, useState, useEffect } from 'react';
import { TodoItem } from './TodoItem';
import './TodoList.css';

let searchTask = "";
let filteredData = [];

export function TodoList() {
    const [title, setTitle] = useState('');
    const [taskList, setTaskList] = useState(JSON.parse(localStorage.getItem('taskList')) || []);
    const [copyTaskList, setCopyTaskList] = useState(JSON.parse(localStorage.getItem('taskList')));
    const [isHide, setIsHide] = useState(JSON.parse(localStorage.getItem('isHide')));
    const inputTitle = useRef();

    useEffect(() => {
        document.title = 'To Do List';
    }, [taskList]);

    useEffect(() => {
        showHideList();
    }, [isHide]);

    const setData = (taskList) => {
        localStorage.setItem('taskList', JSON.stringify(taskList));
    }

    const addChange = () => {
        setTitle(inputTitle.current.value);
    }

    const onAdd = () => {

        if (title.toString().trim()) {
            let tsk = {};
            tsk.id = Math.random();
            tsk.inpAdd = title;
            tsk.createdAt = new Date();
            tsk.isComplete = false;
            setTaskList([...taskList, tsk]);
            setCopyTaskList([...copyTaskList, tsk]);
            setData(copyTaskList);
            console.log(taskList);
            setTitle('');
        }
    }

    const handleNew = (e) => {
        const newestList = [...taskList].sort((a, b) => {
            let d1 = a.createdAt;
            let d2 = b.createdAt;
            if (d1 < d2) {
                return 1;
            }
            else {
                return -1;
            }
        });
        setTaskList(newestList);
    }

    const handleOld = (e) => {
        const oldestList = [...taskList].sort((a, b) => {
            let d1 = a.createdAt;
            let d2 = b.createdAt;
            if (d1 > d2) {
                return 1;
            }
            else {
                return -1;
            }
        });
        setTaskList(oldestList);
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            onAdd();
            setTitle('');
        }
    }

    const searchChange = (e) => {
        searchTask = e.target.value;

        if (searchTask !== '') {
            filteredData = taskList.filter(obj => {
                return obj.inpAdd.toLowerCase().includes(searchTask.toLowerCase());
            });
            setTaskList(filteredData);
            console.log("filteredData ==>> ", filteredData);
        }
        else {
            setTaskList(copyTaskList);
        }
    }

    const handleRemove = (taskId) => {

        const itemDel = taskList.filter((item) => item.id === taskId);
        const newList = taskList.filter((item) => item.id !== taskId);

        let ind = copyTaskList.findIndex((x => x.id === taskId));
        let removed = copyTaskList.splice(ind, 1);

        setData(copyTaskList);
        setTaskList(newList);
    }

    const setCompleted = (isCompleted, taskItem) => {
    
        let findIndex = taskList.findIndex(item => item.id === taskItem.id);
        let findCopyIndex = copyTaskList.findIndex(item => item.id === taskItem.id);

        taskList[findIndex] = taskItem;
        copyTaskList[findCopyIndex] = taskItem;

        setTaskList(taskList);
        setCopyTaskList(copyTaskList);
        setData(copyTaskList);

    }

    const handleHide = async (e) => {
        localStorage.setItem('isHide', e.target.checked);
        await setIsHide(e.target.checked);
        // showHideList();
    }

    const showHideList = () => {
        let shownList;
        const taskListCopy = taskList.slice();

        if (isHide) {
            if (searchTask !== '') {
                shownList = filteredData.filter((item) => item.isComplete === false);
                setTaskList(shownList);
                console.log("shownList ==>> ",shownList);
            }
            else {
                shownList = taskList.filter((item) => item.isComplete === false);
                setTaskList(shownList);
            }
        }
        else {
            if (searchTask !== '') {
               
                setTaskList(filteredData);
                console.log("filteredData(else-if) ==>> ",filteredData);
            }
            else{
                setTaskList(copyTaskList);
            }
            
        }
    }

    return (
        <div className="mainContainer">
            <div className="divSearch">
                <input type="text" name="inpSearch" id="" placeholder="Search Here.." onChange={searchChange} />
            </div>

            <div className="divAdd">
                <input type="text" ref={inputTitle} placeholder="Add Task Here.." value={title} onChange={addChange} onKeyPress={handleKeyPress} />
                <button type="button" onClick={onAdd}>Add</button>
            </div>

            <div className="divFirst">

                <input type="radio" name="rbFirst" value="newest" onClick={handleNew} defaultChecked /><label >Newest First</label>
                <input type="radio" name="rbFirst" value="oldest" onClick={handleOld} /><label >Oldest First</label>

            </div>
            <div className="divHide">
                <input type="checkbox" checked={isHide} onChange={handleHide} /><label>Hide Completed</label>
            </div>
            <div className="divTask">
                <label className="lblTask">Your Tasks</label>
                <div className="divTaskList">
                    {taskList.map((task, i) => {

                        return <TodoItem
                            key={task.id}
                            taskItem={task}
                            onRemove={() => handleRemove(task.id)}
                            check={(isCompleted) => {
                                task.isComplete = isCompleted;
                                setCompleted(isCompleted, task);
                            }} />
                    })}
                </div>
            </div>
        </div>
    );
}